﻿namespace MenuRecursive.Extensions
{
  using System;
  using System.Linq;
  using System.Text;
  using System.Collections.Generic;

  public static class Ext
  {
    private static int RootMenuId = 311;

    /// <summary>
    /// Devolver la raíz del árbol.
    /// </summary>
    /// <param name="menu">Objeto propio del tipo 'MenuDto'.</param>
    /// <returns></returns>
    private static MenuDto GetRoot(this MenuDto menu) => menu.GetNodeWithChildren(x => x.ParentId == null);
    public static IList<MenuDto> GetChildrens(this MenuDto menu, int parentId)
    {
      var _iListNew = new List<MenuDto>();

      if (menu.GetRoot() != null)
      {
        if (RootMenuId == parentId)
          _iListNew = menu.GetChildrenOf(x => x.ParentId == RootMenuId).ToList();
        else
        {
          foreach (var u in menu.Children)
          {
            if (u.GetChildrenOf(x => x.ParentId == parentId).Count > 0)
            {
              _iListNew = u.Children; break;
            }
          }
        }
      }
      else
        _iListNew = null;

      return _iListNew;
    }
    private static MenuDto GetNodeWithChildren(this MenuDto menu, Func<MenuDto, bool> func)
    {
      return new MenuDto
      {
        Id = menu.Id,
        Description = menu.Description,
        LanguageKey = menu.LanguageKey,
        Icon = menu.Icon,
        Form = menu.Form,
        Command = menu.Command,
        ParentId = menu.ParentId,
        Order = menu.Order,
        Enabled = menu.Enabled,
        Children = menu.Children != null ? menu.Children.Where(x => func(x)).Select(x => x.GetNodeWithChildren(func)).ToList() : null
      };
    }
    public static IList<MenuDto> GetChildrenOf(this MenuDto menu, Func<MenuDto, bool> func)
    {
      var item = new MenuDto
      {
        Id = menu.Id,
        Description = menu.Description,
        LanguageKey = menu.LanguageKey,
        Icon = menu.Icon,
        Form = menu.Form,
        Command = menu.Command,
        ParentId = menu.ParentId,
        Order = menu.Order,
        Enabled = menu.Enabled,
        Children = menu.Children != null ? menu.Children.Where(x => func(x)).Select(x => x.GetNodeWithChildren(func)).ToList() : null
      };
      return item.Children;
    }
  }
}
