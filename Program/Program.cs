﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

using MenuRecursive.Extensions;

namespace MenuRecursive
{
  class Program
  {
    // Tomado de: https://stackoverflow.com/questions/33915323/creating-a-tree-with-linq-expression
    static void Main(string[] args)
    {
      string jsonString = @"{ ""id"": 311, ""description"": ""Root"", ""languageKey"": ""lblRoot"", ""icon"": null, ""form"": """", ""command"": null, ""parentId"": 0, ""order"": 0, ""enabled"": true, ""children"":[ { ""id"": 312, ""description"": ""Stock Locator"", ""languageKey"": ""lblStockLocator"", ""icon"": null, ""form"": "" / Menu"", ""command"": null, ""parentId"": 311, ""order"": 0, ""enabled"": true, ""children"": [ { ""id"": 313, ""description"": ""Direct Move"", ""languageKey"": ""lblDirectMove"", ""icon"": null, ""form"": "" / StockLocator / DirectMove"", ""command"": ""shift + 1"", ""parentId"": 312, ""order"": 0, ""enabled"": true, ""children"": [] } ] }, { ""id"": 314, ""description"": ""Picking"", ""languageKey"": ""lblPicking"", ""icon"": null, ""form"": "" / Menu"", ""command"": null, ""parentId"": 311, ""order"": 0, ""enabled"": true, ""children"": [ { ""id"": 315, ""description"": ""Regular Pick"", ""languageKey"": ""lblRegularPicking"", ""icon"": null, ""form"": "" / Reciving / NormalReciving"", ""command"": ""shift + 2"", ""parentId"": 314, ""order"": 0, ""enabled"": true, ""children"": []}]}]}";
      var menuDtoAll = MenuDto.FromJson(jsonString);

      var result33 = menuDtoAll.GetChildrens(311);
      var result31 = menuDtoAll.GetChildrens(312);
      var result32 = menuDtoAll.GetChildrens(314);

      Console.WriteLine("Saludos a todos"); Console.ReadLine();
    }
  }
}
