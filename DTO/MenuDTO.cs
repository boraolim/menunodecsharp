﻿namespace MenuRecursive
{
  using System;
  using System.Text;
  using System.Globalization;
  using System.Collections.Generic;
  
  using Newtonsoft.Json;
  using Newtonsoft.Json.Converters;
  using System.Linq;

  public partial class MenuDto
  {
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("languageKey")]
    public string LanguageKey { get; set; }

    [JsonProperty("icon")]
    public object Icon { get; set; }

    [JsonProperty("form")]
    public object Form { get; set; }

    [JsonProperty("command")]
    public object Command { get; set; }

    [JsonProperty("parentId")]
    public int? ParentId { get; set; }

    [JsonProperty("order")]
    public int? Order { get; set; }

    [JsonProperty("enabled")]
    public bool Enabled { get; set; }

    [JsonProperty("children")]
    public List<MenuDto> Children { get; set; }
  }

  public partial class MenuDto
  {
    public static MenuDto FromJson(string json) => JsonConvert.DeserializeObject<MenuDto>(json, MenuRecursive.Converter.Settings);

    public bool InParentId(int? parentId)
    {
      if (parentId == null)
      {
        throw new ArgumentNullException("ParentId");
      }
      var inRole = Children.Where(u => u.ParentId == parentId);
      if (inRole == null & Children != null)
      {
        return Children.Any(child => child.InParentId(parentId));
      }
      return (inRole.Count() > 0);
    }


  }

  public static class Serialize
  {
    public static string ToJson(this List<MenuDto> self) => JsonConvert.SerializeObject(self, MenuRecursive.Converter.Settings);
  }

  internal static class Converter
  {
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
      MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
      DateParseHandling = DateParseHandling.None,
      Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
    };
  }
}
